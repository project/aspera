########################################################################
#  Aspera Drupal Lookup  Script  - GPLv2 															 #
#                                                                      #
#  Description: Lookup metadata about show node when file is uploaded  #
#  Append Cablecast ID to filename
#  Move renamed file to SD or HD directory                             #
########################################################################


BEGIN {
    unshift(@INC, "../lib/perl");
    unshift(@INC, "/usr/local/aspera/lib/perl");
    unshift(@INC, "/opt/aspera/lib/perl");
    unshift(@INC, "/Library/Aspera/lib/perl");
}

use strict;
use aspera::logger;
use aspera::paths;

use Sys::Hostname;
use Mail::Sendmail;
use XML::Simple;                # to parse config file

use MIME::Base64;

# Custom arguments
my $BASEURL = "http://[YOUR-DRUPAL-URL]/aspera-show-csv-lookup/";
my $BASEPATH = "/ifs/home/asp1/";

# Common arguments
my $TYPE          = $ENV{"TYPE"};
my $STARTSTOP     = $ENV{"STARTSTOP"};
my $SESSIONID     = $ENV{"SESSIONID"};
my $STATE         = $ENV{"STATE"};
my $ERRCODE       = $ENV{"ERRCODE"};
my $ERRSTR        = $ENV{"ERRSTR"};
my $DIRECTION     = $ENV{"DIRECTION"};

# Session arguments
my $SOURCE        = $ENV{"SOURCE"};
my $TARGET        = $ENV{"TARGET"};
my $PEER          = $ENV{"PEER"};
my $USERID        = $ENV{"USERID"};
my $USER          = $ENV{"USER"};
my $TARGETRATE    = $ENV{"TARGETRATE"};
my $MINRATE       = $ENV{"MINRATE"};
my $RATEMODE      = $ENV{"RATEMODE"};
my $SECURE        = $ENV{"SECURE"};
my $LICENSE       = $ENV{"LICENSE"};
my $PEERLICENSE   = $ENV{"PEERLICENSE"};
my $FILECOUNT     = $ENV{"FILECOUNT"};
my $TOTALBYTES    = $ENV{"TOTALBYTES"};
my $TOTALSIZE     = $ENV{"TOTALSIZE"};
my $FILE1         = $ENV{"FILE1"};
my $FILE2         = $ENV{"FILE2"};
my $FILELAST      = $ENV{"FILELAST"};
my $TOKEN         = $ENV{"TOKEN"};
my $COOKIE        = $ENV{"COOKIE"};

# Transfer arguments
my $FILE          = $ENV{"FILE"};
my $SIZE          = $ENV{"SIZE"};
my $STARTBYTE     = $ENV{"STARTBYTE"};
my $RATE          = $ENV{"RATE"};
my $DELAY         = $ENV{"DELAY"};
my $LOSS          = $ENV{"LOSS"};
my $REXREQS       = $ENV{"REXREQS"};
my $OVERHEAD      = $ENV{"OVERHEAD"};

$ENV{"NEWLINE"} = "\n";

# Globals
my $READSTATE;
my %MAILLISTS;
my $FILTERCOUNT = 0;
my $BODYTEXT;
my @FMAILLISTS;
my @FUSER;
my @FDIRECTION;
my @FSRCIP;
my @FDESTIP;
my @FSOURCE;
my @FTARGETDIR;
my @FBODYPREFIX;
my @FSUBJECTPREFIX;
my @FTOTALBYTESOVER;
my @FSENDONSTART;
my @FSENDONSTOP;
my @FSENDONSESSION;
my @FSENDONFILE;
my $MYIP = join(".", unpack("C4", (gethostbyname(hostname()))[4]));
my $SRCIP;
my $DSTIP;
my %MAIL;
my $CONFFILE = "aspera.conf";
my $CONFPATH = aspera::paths::getEtcDir()."/$CONFFILE";
my $DEBUG = 0;

#wait until the upload is a success
if (lc($STATE) == "success") {
  #double check that a file is actually being processed
  if (defined $FILE) {
		my @fileparts = split('/', $FILE);
		my $filename = $fileparts[-1];

		my $url = $BASEURL . $filename;
		my $result = `curl $url`;
	
		&writelog("Drupal lookup script: start processing $filename");

		my @actionparts = split(',', $result);
		my $cablecastid = $actionparts[0];
		my $destdir = $actionparts[-1];

		#clean up directory... removing line break
		$destdir =~ s/\r?\n$//;

		#confirm the filename was found in Drupal
		if ($destdir == 'HD' || $destdir == 'SD') {
			#confirm cablecastid is number
			if ($cablecastid =~ /^[+-]?\d+$/ ) {

				my $updatedfilename = $cablecastid . '-1-' . $filename;
				#sanitize the filename
				$updatedfilename =~ s/[^A-Za-z0-9\-\.]//g; 
				my $mv = $BASEPATH . $filename . ' ' . $BASEPATH . $destdir . '/' . $updatedfilename;
				my $error = `mv $mv`;
				&writelog("Drupal lookup script: action - mv $mv");
	
				#&writelog("Drupal lookup script: error - $error");
			} else {
				#there was an error... what do we know
				#&writelog("Drupal lookup script: curl result - $result");
				#&writelog("Drupal lookup script: cablecastid - $cablecastid");
				#&writelog("Drupal lookup script: direcotry - $destdir");
			}
		}
		
	&writelog("Drupal lookup script: end processing $filename");
	
  }
}


sub writelog {
  &AsperaLogger::writelog($_[0], "aspera-drupal-lookup.pl");
}


